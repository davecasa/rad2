#!/usr/bin/env python

import rospy
import struct
import binascii
import serial

from rad2_msgs.msg import *

CRC_TABLE = [0x00,0x07,0x0E,0x09,0x1C,0x1B,0x12,0x15,0x38,0x3F,
0x36,0x31,0x24,0x23,0x2A,0x2D,0x70,0x77,0x7E,0x79,
0x6C,0x6B,0x62,0x65,0x48,0x4F,0x46,0x41,0x54,0x53,
0x5A,0x5D,0xE0,0xE7,0xEE,0xE9,0xFC,0xFB,0xF2,0xF5,
0xD8,0xDF,0xD6,0xD1,0xC4,0xC3,0xCA,0xCD,0x90,0x97,
0x9E,0x99,0x8C,0x8B,0x82,0x85,0xA8,0xAF,0xA6,0xA1,
0xB4,0xB3,0xBA,0xBD,0xC7,0xC0,0xC9,0xCE,0xDB,0xDC,
0xD5,0xD2,0xFF,0xF8,0xF1,0xF6,0xE3,0xE4,0xED,0xEA,
0xB7,0xB0,0xB9,0xBE,0xAB,0xAC,0xA5,0xA2,0x8F,0x88,
0x81,0x86,0x93,0x94,0x9D,0x9A,0x27,0x20,0x29,0x2E,
0x3B,0x3C,0x35,0x32,0x1F,0x18,0x11,0x16,0x03,0x04,
0x0D,0x0A,0x57,0x50,0x59,0x5E,0x4B,0x4C,0x45,0x42,
0x6F,0x68,0x61,0x66,0x73,0x74,0x7D,0x7A,0x89,0x8E,
0x87,0x80,0x95,0x92,0x9B,0x9C,0xB1,0xB6,0xBF,0xB8,
0xAD,0xAA,0xA3,0xA4,0xF9,0xFE,0xF7,0xF0,0xE5,0xE2,
0xEB,0xEC,0xC1,0xC6,0xCF,0xC8,0xDD,0xDA,0xD3,0xD4,
0x69,0x6E,0x67,0x60,0x75,0x72,0x7B,0x7C,0x51,0x56,
0x5F,0x58,0x4D,0x4A,0x43,0x44,0x19,0x1E,0x17,0x10,
0x05,0x02,0x0B,0x0C,0x21,0x26,0x2F,0x28,0x3D,0x3A,
0x33,0x34,0x4E,0x49,0x40,0x47,0x52,0x55,0x5C,0x5B,
0x76,0x71,0x78,0x7F,0x6A,0x6D,0x64,0x63,0x3E,0x39,
0x30,0x37,0x22,0x25,0x2C,0x2B,0x06,0x01,0x08,0x0F,
0x1A,0x1D,0x14,0x13,0xAE,0xA9,0xA0,0xA7,0xB2,0xB5,
0xBC,0xBB,0x96,0x91,0x98,0x9F,0x8A,0x8D,0x84,0x83,
0xDE,0xD9,0xD0,0xD7,0xC2,0xC5,0xCC,0xCB,0xE6,0xE1,
0xE8,0xEF,0xFA,0xFD,0xF4,0xF3]

class twogController:
    def __init__(self):
        rospy.init_node('twog')
        self.packets = dict()
        self.port = '/dev/ttyUSB0'
        self.baudrate = 115200
        self.serial_timeout = 0.05
        self.dt = 0.2
        self.init_packets()
        self.init_serial()
        
        self.status_msg = TwoGStatus()
        self.status_pub = rospy.Publisher("twog_status", TwoGStatus, queue_size=10)
        self.command_sub = rospy.Subscriber("twog_command", TwoGCommand, self.command_cb)
        
        rospy.Timer(rospy.Duration(self.dt), self.timer_cb, oneshot=False)

    def init_serial(self):
        try:
            self.ser = serial.Serial(port=self.port, baudrate=self.baudrate,
                           bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
                           stopbits=serial.STOPBITS_ONE, timeout=self.serial_timeout,
                           xonxoff=0, rtscts=0)
            self.ser.nonblocking()
        except serial.serialutil.SerialException as msg:
            rospy.logfatal('Failed to open %s (%d): %s' % (self.port, self.baudrate, msg.message))
            exit()
        self.symbol_duration = rospy.Duration.from_sec(10./float(self.baudrate))

    def init_packets(self):
        self.packets[0x50] = self.system_status_information
        self.packets[0x41] = self.acknowledgement
        self.packets[0x44] = self.system_configuration_2
        self.packets[0x48] = self.velocity_information
        
    def __del__(self):
        self.motor_configuration_off()
        self.read()
        #self.ser.close()
    
    def command_cb(self, msg):
        self.set_system_configuration_2(0.001, 0.0013, 0.001, 4350, 1000, 144000, 1440000, msg.target_velocity, 1.0, 0)
        if msg.enable:
            self.motor_configuration_on()
            self.read()
        else:
            self.motor_configuration_off()
            self.read()
        self.position_setpoint_fixed_velocity_extended(msg.target_velocity, msg.target_degrees, 1, b'00')
        self.read()
    
    def timer_cb(self, event):
        self.status_msg.header.stamp = rospy.Time.now()
        self.request_system_status_information()
        self.read()
        self.request_velocity_information()
        self.read()
        #rot.request_system_configuration_2()
        #rot.read()
        self.status_pub.publish(self.status_msg)
    
    def request_system_status_information(self):
        self.send(1, b'70')
    
    def request_velocity_information(self):
        self.send(1, b'68')
        
    def request_system_configuration_2(self):
        self.send(1, b'64')
    
    def velocity_information(self, payload):
        payload = binascii.unhexlify(payload)
        (packet_type, motor_velocity, shaft_velocity) = struct.unpack('>cll', payload)
        motor_velocity = float(motor_velocity) * 1e-3 / 360.
        shaft_velocity = float(shaft_velocity) * 1e-3 / 360.
        #print(motor_velocity)
        #print(shaft_velocity)
        self.status_msg.velocity = shaft_velocity
        print('Velocity: %.3f RPM' % shaft_velocity)
    
    def position_setpoint_fixed_velocity(self, velocity, position_setpoint):
        velocity *= 1e3
        position_setpoint *= 1e3
        data = struct.pack('>Ll', int(velocity), int(position_setpoint))
        #print(data)
        self.send(9, b'55'+binascii.hexlify(data))
    
    def position_setpoint_fixed_velocity_extended(self, velocity, position_setpoint, stop_threshold, stop_behavior):
        velocity *= 1e3
        position_setpoint *= 1e3
        stop_threshold *= 1e3
        data = struct.pack('>LlL', int(velocity), int(position_setpoint), int(stop_threshold))
        self.send(14, b'4B'+binascii.hexlify(data)+stop_behavior)

    def velocity_setpoint(self, velocity_setpoint):
        velocity_setpoint *= 1e3
        data = struct.pack('>L', int(velocity_setpoint))
        self.send(2, b'57'+binascii.hexlify(data))

    def motor_configuration_on(self):
        self.send(2, b'5801')
    
    def motor_configuration_off(self):
        self.send(2, b'5800')
        
    def system_status_information(self, payload):
        payload = binascii.unhexlify(payload)
        (packet_type, motor_status, motor_direction, absolute_position, motor_revolutions, total_degrees, temperature1, temperature2, voltage, current, reserved) = struct.unpack('>cBBiiibbihB', payload)
        absolute_position = float(absolute_position) *1e-3
        total_degrees = float(total_degrees) * 1e-3
        voltage = float(voltage) *1e-3
        current = float(current) * 1e-3
        self.status_msg.total_degrees = total_degrees
        self.status_msg.temp1 = temperature1
        self.status_msg.temp2 = temperature2
        self.status_msg.voltage = voltage
        self.status_msg.current = current
        print('status %d, direction %d, position %.3f, revolutions %d, degrees %.3f, temp1 %d, temp2 %d, voltage %.3f, current %.3f' % (motor_status, motor_direction, absolute_position, motor_revolutions, total_degrees, temperature1, temperature2, voltage, current))
    
    def set_system_configuration_2(self, pgain, igain, dgain, intmax, dclamp, posscale, velscale, targetvel, setpointthreshold, setpointbehavior):
        targetvel = int(targetvel * 360. * 1e3)
        setpointthreshold = int(setpointthreshold * 1e3)
        data = struct.pack('>fffffLLLLLB', pgain, igain, dgain, intmax, dclamp, posscale, velscale, 0, int(targetvel), int(setpointthreshold), setpointbehavior)
        self.send(42, b'44'+binascii.hexlify(data))

    def system_configuration_2(self, payload):
        payload = binascii.unhexlify(payload)
        (packet_type, pgain, igain, dgain, intmax, dclamp, posscale, velscale, reserved, targetvel, setpointthreshold, setpointbehavior) = struct.unpack('>cfffffLLLLLB', payload)
        print('pgain %f, igain %f, dgain %f, intmax %f, dclamp %f' % (pgain, igain, dgain, intmax, dclamp))
        print('posscale %f, velscale %f, targetvel %f, setpointthreshold %f, setpointbehavior %d' % (posscale, velscale, targetvel*1e-3 / 360, setpointthreshold*1e-3, setpointbehavior))
    
    def acknowledgement(self, payload):
        payload = binascii.unhexlify(payload)
        (packet_type, model_identifier) = struct.unpack('>cB', payload)
        id0 = (model_identifier & 0b10000000) >> 7
        id1 = (model_identifier & 0b01100000) >> 5
        series = (model_identifier & 0b00011110) >> 1
        gen = (model_identifier & 0b00000001)
        output = 'Ack. '
        output += 'Rot. ' if id0 else 'Lin. '
        output += 'Valve. ' if id1 else 'Standard. '
        if series == 0b0000:
            output += '2000. '
        elif series == 0b0001:
            output += '3500. '
        elif series == 0b0010:
            output += '4000. '
        elif series == 0b0011:
            output += 'HPU. '
        elif series == 0b0100:
            output += '6000. '
        output += 'Gen 2.' if gen else 'Gen 1.'
        print(output)
        
    def send(self, length_int, payload):
        length_hex = bytes(self.length2hex(length_int), 'utf-8')
        #print(type(b'('))
        #print(type(length_hex))
        #print(type(payload))
        #print(type(self.crc(length_hex+payload)))
        #print(type(b')'))
        cmd = b'(' + length_hex + payload + self.crc(length_hex+payload) + b')'
        #rospy.loginfo('tx: %s' % cmd)
        self.ser.write(cmd)

    def read(self):
        startchar = self.ser.read(1)
        starttime = rospy.Time.now() - self.symbol_duration
        if startchar != b'(':
            rospy.logwarn('Invalid start character: %s' % startchar)
            return
        length_hex = self.ser.read(2)
        length_int = int(length_hex, 16)
        payload = self.ser.read(length_int * 2)
        crc = self.ser.read(2)
        endchar = self.ser.read(1)
        if endchar != b')':
            rospy.logwarn('Invalid end character: %s' % endchar)
            return
        #rospy.loginfo('rx: %s' % (startchar + length_hex + payload + crc + endchar))
        crc_calc = self.crc(length_hex + payload)
        if crc != crc_calc:
            rospy.logwarn('Invalid CRC: Expected %s, received %s' % (crc_calc, crc))
            return
        packet_type = int(payload[0:2], 16)
        if packet_type in self.packets:
            self.packets[packet_type](payload)
        else:
            rospy.logwarn('Undefined packet type %#X' % packet_type)
        
        
    def length2hex(self, length):
        return hex(length)[2:].upper().zfill(2)

    def crc(self, bytes_hex):
        crc = 0
        #print(bytes_hex)
        bytes_bin = binascii.unhexlify(bytes_hex)
        #string_bin = binascii.unhexlify(bytes)
        for byte in bytes_bin:
            crc = CRC_TABLE[crc ^ byte]
        #a = hex(crc)[2:].upper().zfill(2)
        #a = '%X' % crc
        #return bytes('%X' % crc, 'utf-8')
        return bytes(hex(crc)[2:].upper().zfill(2), 'utf-8')


if __name__ == "__main__":
    rot = twogController()
    #rot.velocity_setpoint(2.0)
    #speed = 30
    #rot.set_system_configuration_2(0.001, 0.0013, 0.001, 4350, 1000, 144000, 1440000, speed, 1.0, 0)
    #rot.read()
    #rot.motor_configuration_on()
    #rot.read()
    #rot.position_setpoint_fixed_velocity_extended(1, 0000, 5, b'00')
    #rot.read()
    while not rospy.is_shutdown():
        rospy.spin()
        #rot.request_system_status_information()
        #rot.read()
        #rot.request_velocity_information()
        #rot.read()
        #rot.request_system_configuration_2()
        #rot.read()
        #time.sleep((0.1))
